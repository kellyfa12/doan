# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

"Hệ thống quản lý nghiên cứu khoa học(NCKH) tại trường Đại học Đại Nam" được xây dựng bởi 2 thành viên là: Đỗ Văn Hoàng & Cao Hữu Phương dưới sự hướng dẫn của Thạc sĩ: Phạm Công Hòa
Hệ thống được phát triển trên nền tảng Laravel PHP Framework 5.2 và vẫn đang trong quá trình xây dựng.


## Official Documentation

Tài liệu để lập trình vui lòng xem tại [Laravel website](http://laravel.com/docs).

## Contributing

Các đóng góp cho tài liệu [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

Nếu bạn phát hiện ra một lỗ hổng bảo mật trong Laravel, xin vui lòng gửi e-mail cho Taylor Otwell tại taylor@laravel.com. Tất cả các lỗ hổng bảo mật sẽ được giải quyết kịp thời.

## License

Tất cả các plugins, thư viện cũng như giao diện được xây dựng trên nền Bootstrap 3. Tất cả bản quyền của các thư viện trên thuộc về Almsaeed Studio [MIT license](http://opensource.org/licenses/MIT).
